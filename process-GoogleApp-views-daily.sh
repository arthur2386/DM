#!/bin/bash

echo "Process liverail files daily, first day to process: ${1}, last day to process ${2} , with the time sleep  ${3}"
echo "DAILY"

days=${2}
days=$(($days + 1))

url="https###redo_aggregate&period=201509"
current_url=""

current_day=${1}

while :
do
    sleep ${3}
    if [ ${#current_day} -eq 1 ];then
        current_day=0$current_day

    fi

    current_url=$url$current_day

    echo $current_url
    curl "$current_url"
    if [ ${#current_day} -eq 2 ] && [ ${current_day:0:1} = 0  ];then
        current_day=${current_day:1:1}
    fi


    current_day=$(($current_day + 1))
    echo "Current day :"
    echo $current_day


    if [ $current_day -eq $days ];then
	exit 1
    fi


done

#echo "MONTHLY"
#curl "$url"

echo "done"
