#!/usr/bin/env python
from multiprocessing import Process
from time import time
import random
import socket
import os.path
import subprocess
import uuid
import hashlib
from pprint import pprint

from couchbase import Couchbase
from couchbase.exceptions import CouchbaseError


IDS_COUNT = 100000
CARDINALITY = 100
BENCH_COUNT = 200000
PROCESS_COUNT = 16
UNIQUE_KEY = True
USE_HASH = True
HASH_KEYS = False
NAMESPACE = 'test'
SET = 'set'


def get_client():
    return Couchbase.connect(bucket='test', host='localhost')


def get_hash(value):
    return hashlib.sha1(value).hexdigest()

def bench_callable(client, process_id, nb,
        unique_key=True, use_hash=True, hash_keys=True):

    client = get_client()
    print (nb)
    for i in xrange(nb):

        id_ = uuid.uuid4() if unique_key else random.randint(1, IDS_COUNT)
        ts = 123456789

        # pipeline = client.pipeline()
        # with pipeline:
        #     for j in xrange(CARDINALITY):
        #         key = 'ts=%s&event=play&video_id=%s&card=%s' % (ts, id_, j)
        #         if hash_keys:
        #            key = get_hash(key)
                #client.incr(key, amount=1, initial=1)
       # keys = ['ts=%s&event=play&video_id=%s&card=%s' % (ts, id_, j)
         #       for j in xrange(CARDINALITY)]
        #res = client.incr_multi(keys,amount=1)
        #pprint(res)
        # Check the write
        # if len(pipeline.results) != CARDINALITY:
        #     print("ERROR : pipelin results not egal to CARDINALITY")

def multi_process(bench_count, process_count,
        unique_key=False, use_hash=False, hash_keys=False):
    client = get_client()
    client_processes = process_count
    proc_bench_count = bench_count / CARDINALITY / client_processes
    list_process = []
    started = time()


    #bench_callable(client, 111, proc_bench_count#Z, unique_key, use_hash, hash_keys)

    for process_id in range(client_processes):
        kwargs = {
            'client': client,
            'process_id': process_id,
            'nb': proc_bench_count,
            'unique_key': unique_key,
            'use_hash': use_hash,
            'hash_keys': hash_keys,
            }
        proc = Process(target=bench_callable, kwargs=kwargs)
        list_process.append(proc)
        proc.start()




    for proc in list_process:
        proc.join()

    elapsed = time() - started
    incr_count = proc_bench_count * CARDINALITY * len(list_process)
    print 'processed %d incr in %.03f sec (%.03f / sec) with %d processes' % (
            incr_count, elapsed, incr_count / elapsed, len(list_process))
    return int(incr_count / elapsed)

def main():
    res = multi_process(bench_count=BENCH_COUNT, process_count=PROCESS_COUNT,
            unique_key=UNIQUE_KEY, use_hash=USE_HASH, hash_keys=HASH_KEYS)
    print '%s incr/sec' % res


if __name__ == '__main__':
    main()
