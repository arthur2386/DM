<?php

$content = implode(' ', array('information', 'actualité en direct en france'));

try
{
    $url = 'http://langdetector.###';
    $data = array(
        'id'      => 1234,
        'method'  => 'detect',
        'jsonrpc' => '2.0',
        'params'  => array(
            'content' => $content
        )
    );

    $curlClient->init($url);
    $curlClient->setPinbaTags(
        [
            'group'     => 'curl',
            'method'    => 'check_for_language',
            'namespace' => 'video',
            'curl'      => $url
        ]
    );
    $curlClient->setOptions(
        array(
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT        => 5,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_POSTFIELDS     => json_encode($data)
        )
    );
    $response = $this->curlClient->execute();
    $curlClient->close();

    $language = json_decode($response)->result;

    print($language);
}
catch (Exception $e)
{
    return -1;
}
